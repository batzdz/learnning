package list;

import java.util.Iterator;
import java.util.NoSuchElementException;

// 顺序表 记录当前走到那个位置
public class MyArrayListIterator<E> implements Iterator {
    private int currentIndex;

    private int size;

    private E[] el;

    public MyArrayListIterator(int size, E[] el) {
        currentIndex = 0;
        this.size = size;
        this.el = el;
    }

    @Override
    public boolean hasNext() {
        return currentIndex < size;
    }

    @Override
    public Object next() {
        if(currentIndex >= size){
            throw new NoSuchElementException();
        }
        E currentElement = el[currentIndex++];
        return currentElement;
    }
}
