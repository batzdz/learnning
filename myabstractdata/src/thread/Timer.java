package thread;

import java.util.concurrent.PriorityBlockingQueue;

public class Timer { // 定时器

    static class Task implements Comparable<Task>{ // 任务类
        private Runnable command; // 任务
        private long time;        // 任务何时执行

        public Task(Runnable command, long after) { // after 相对时间
            this.command = command;
            this.time = System.currentTimeMillis() + after;
        }

        @Override // 为了可以放入优先阻塞队列中
        public int compareTo(Task o) {
            return (int) (this.time - o.time);
        }

        public void run(){
            command.run();
        }
    }


    private PriorityBlockingQueue<Task> queue = new PriorityBlockingQueue<>();  // 优先阻塞队列
    private Object mailBox = new Object();


    static class Worker extends Thread{ // 监视线程 用来监视取任务执行
        private PriorityBlockingQueue<Task> queue;
        private Object mailBox;
        public Worker (PriorityBlockingQueue<Task> queue,Object mailBox){
            this.queue = queue;
            this.mailBox = mailBox;
        }
        @Override
        public void run() {
            while (true) {
                Task task = null;
                try {
                        task = queue.take();
                        long currentTimeMillis = System.currentTimeMillis();
                        if (task.time > currentTimeMillis) {
                            synchronized (mailBox) {
                                try {
                                    queue.put(task);
                                    mailBox.wait(task.time - currentTimeMillis);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }else {
                            task.run();
                        }
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }


    private Worker worker;
    public Timer(){
        worker = new Worker(queue,mailBox);
        worker.start();
    }

    public void schedule(Task task){
        queue.put(task);
        synchronized (mailBox){
            mailBox.notify();
        }
    }

    public void schedule(Runnable command,long after){
        queue.offer(new Task(command,after));
        synchronized(mailBox){
            mailBox.notify();
        }
    }

    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("靓仔你是真的帅!");
                timer.schedule(this,3000);
            }
        },3000);
    }

}
