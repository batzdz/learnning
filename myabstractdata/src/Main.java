import java.sql.Time;
import java.util.*;
// 一个圈子内最多的人数
// 这里用的应该是 分治的思想 
public class Main{
    static int ans = 0;
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        // 有  n 组输入实例
        for(int i = 0; i < n; i++){
            int rows = sc.nextInt();
            int[][] groups = new int[rows][2];
            for(int j = 0; j < rows; j++){
                for(int k = 0; k < 2; k++){
                    groups[j][k] = sc.nextInt();
                }
            }
            Set<Integer> set = new HashSet<>();
            process(groups,set,new boolean[rows],0); // dfs
            System.out.println(ans);
        }
    }

    // 获取到数组中的最长连续数序列的长度
    // 首先想到的是穷举 ： 取出 一个区间 作为 标记区间
    // 遍历 其余部分 如果其余部分中 有与 标记区间 相同的元素 则将 那个不相同的 加入到标记区间 得到最大的标记区间长度
    // 由于无论取得是那个 都要和 其余元素进行 操作 所以 取的位置并不影响结果
    static void process(int[][] groups,Set<Integer> set,boolean[] visited,int index){

    }
}