package day01;
import java.util.*;
public class Main {
    // 输出字典序最小的那组意思是枚举的时候从小到大枚举?
    static List<Integer> list = new ArrayList<>();
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int sum = sc.nextInt();

        dfs(N,sum);
        for(int num : list) {
            System.out.print(num + " ");
        }
    }

    static void dfs(int N,int sum) {
        if(sum  < 0 || (list.size() >= N && sum > 0)) return;
        if(list.size() == N && sum == 0) {
            return ;
        }
        for(int i = 1; i <= sum; i++) {
            list.add(i);
            dfs(N,sum-i);
            list.remove(list.size() -1);
        }
    }

}
