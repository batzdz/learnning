package com.example.mybatisplusdemo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Type;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
public class User {
   @TableField(value = "user_id") // 对于主键 这个设置就没用!
   @TableId(type = IdType.AUTO) // 设置主键自增长策略!
   private int user_id;

   @TableField(value = "name") // 表字段映射!
   private String u_name;
   @TableField(select = false) // 查询时不返回该字段的值
   private String pass;

   @TableField(exist = false) // 表示这个字段不被用作数据库操作!
   private String email; // 在数据库表中没有这个字段! 但是user 对象中有
}
