package tree;

public class TreeNode<E>{
    public E val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(E val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "val=" + val.toString() +
                '}';
    }
}
