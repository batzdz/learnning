public class UseArrayList {
    // assert ：断言
    private static void assertTrue(boolean condition) {
        if (!condition) {
            throw new RuntimeException();
        }
    }

    public static void main(String[] args) {
        MyArrayList arrayList = new MyArrayList();
        arrayList.check();
        assertTrue(arrayList.size() == 0);


        arrayList.add(0, 100);
        arrayList.check();
        assertTrue(arrayList.size() == 1);
        assertTrue(arrayList.get(0) == 100);


        arrayList.add(0, 200);
        arrayList.check();
        assertTrue(arrayList.size() == 2);
        assertTrue(arrayList.get(0) == 200);
        assertTrue(arrayList.get(1) == 100);


        arrayList.add(1, 300);

        arrayList.remove(1);
        arrayList.check();
        assertTrue(arrayList.size() == 2);
        assertTrue(arrayList.get(0) == 200);
        assertTrue(arrayList.get(1) == 100);


    }




    public static void main2(String[] args) {
        MyArrayList arrayList = new MyArrayList();
        arrayList.check();
        assertTrue(arrayList.size() == 0);

        arrayList.add(0, 100);
        arrayList.check();
        assertTrue(arrayList.size() == 1);
        assertTrue(arrayList.get(0) == 100);

        arrayList.add(0, 200);
        arrayList.check();
        assertTrue(arrayList.size() == 2);
        assertTrue(arrayList.get(0) == 200);
        assertTrue(arrayList.get(1) == 100);

        arrayList.add(1, 300);
        arrayList.check();
        assertTrue(arrayList.size() == 3);
        assertTrue(arrayList.get(0) == 200);
        assertTrue(arrayList.get(1) == 300);
        assertTrue(arrayList.get(2) == 100);

        arrayList.add(3, 400);
        arrayList.check();
        assertTrue(arrayList.size() == 4);
        assertTrue(arrayList.get(0) == 200);
        assertTrue(arrayList.get(1) == 300);
        assertTrue(arrayList.get(2) == 100);
        assertTrue(arrayList.get(3) == 400);
    }

    public static void main1(String[] args) {
        MyArrayList arrayList = new MyArrayList();  // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 0);


        arrayList.add(1);                           // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 1);
        assertTrue(arrayList.get(0) == 1);


        arrayList.add(2);                           // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 2);
        assertTrue(arrayList.get(0) == 1);
        assertTrue(arrayList.get(1) == 2);


        arrayList.add(3);                           // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 3);
        assertTrue(arrayList.get(0) == 1);
        assertTrue(arrayList.get(1) == 2);
        assertTrue(arrayList.get(2) == 3);


        arrayList.add(4);                           // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 4);
        assertTrue(arrayList.get(0) == 1);
        assertTrue(arrayList.get(1) == 2);
        assertTrue(arrayList.get(2) == 3);
        assertTrue(arrayList.get(3) == 4);


        arrayList.add(5);                           // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 5);
        assertTrue(arrayList.get(0) == 1);
        assertTrue(arrayList.get(1) == 2);
        assertTrue(arrayList.get(2) == 3);
        assertTrue(arrayList.get(3) == 4);
        assertTrue(arrayList.get(4) == 5);
    }
}
