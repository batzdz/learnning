package process;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Task {
    public static void main(String[] args) throws IOException, InterruptedException {
        // JVM  中独一份,单例的!
        Runtime runtime = Runtime.getRuntime();

        // 进程创建同时会打开三个文件: 1.标准输入(键盘). 2. 标准输出(显示器). 3. 标准错误.(显示器)
        Process process = runtime.exec("javac");

        /**
         * todo : 此时父进程和子进程是并发的;
         *
         */
        // 获取进程的标准输出和标准错误,把这里的内容写到两个文件中.
        // 获取标准输入,从文件中读,把子进程的标准输出给读出来!
        InputStream stdoutFrom = process.getInputStream();
        FileOutputStream stdoutTo = new FileOutputStream("stdout.txt");
        while(true){
            int ch = stdoutFrom.read();
            if(ch == -1) break;
            stdoutTo.write(ch);
        }
        stdoutFrom.close();
        stdoutTo.close();

        // 获取标准错误文件,从这个文件中读就可以把子进程的标准错误給读出来!
        InputStream stderrFrom = process.getErrorStream();
        FileOutputStream stderrTo= new FileOutputStream("stderr.txt");
        while(true){
            int ch = stderrFrom.read();
            if(ch == -1) break;
            stderrTo.write(ch);
        }
        stderrFrom.close();
        stderrTo.close();


        // 为了检测子进程的 执行情况, 执行结果;
        // 需要父进程等待子进程的结束;
        int exitCode = process.waitFor();
        System.out.println(exitCode);
    }
}
