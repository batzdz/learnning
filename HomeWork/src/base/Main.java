package base;

public class Main {
    public static void main(String[] args) {
        System.out.println(numbersOf9());
    }

    // 数一下[1..100] 数字 9 出现的次数
    static int numbersOf9(){
        int ans = 0;
        for(int i = 9; i <= 100; i ++){
            int num = i;
            while(num != 0){
                if(num % 10 == 9) ans++;
                num /= 10;
            }
        }
        return ans;
    }

    // 输出 1000 - 2000 之间所有的闰年
    // TODO
    static void print_leap_year(){
        int i = 1000;

        for(; i <= 2000; i++){
            if((i%4 == 0 && i % 100 != 0) || (i%400 == 0)){
                System.out.print(i+" ");
            }
        }
    }


    // 打印素数 [1...100]
    static void printSu(){
        for(int i = 1; i < 100; i++){
            if(isPrime(i)){
                System.out.print(i+" ");
            }
        }
    }
    static boolean isPrime(int num){
        for(int i = 2; i < num; i++){
            if(num%i == 0) return false;
        }
        return true;
    }
}
