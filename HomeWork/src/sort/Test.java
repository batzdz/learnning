package sort;

import java.util.Arrays;
import java.util.Random;

public class Test {
    public static int[] buildRandomArr(int size){
        Random random = new Random();
        int[] arr = new int[size];
        for(int i = 0; i < size; i++){
            arr[i] = random.nextInt();
        }
        return arr;
    }

    public static int[] buildEqualArr(int size){
        Random random = new Random();
        int val = random.nextInt();
        int[] arr = new int[size];
        for(int i = 0; i < size; i++){
            arr[i] = val;
        }
        return arr;
    }

    public static int[] buildSortedArr(int size){
        Random random = new Random();
        int val = random.nextInt();
        int[] arr = new int[size];
        for(int i = 0; i < size; i++){
            arr[i] = val;
        }
        Arrays.sort(arr);
        return arr;
    }

    public static int[] buildOfArr(int size){
        int half = size / 2;
        int[] array = buildSortedArr(size);
        for (int i = 0; i < half; i++) {
            int t = array[i];
            array[i] = array[size - 1 - i];
            array[size - 1 - i] = t;
        }
        return array;
    }
}
