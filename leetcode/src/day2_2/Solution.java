package day2_2;

import java.util.ArrayList;
import java.util.List;

class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
  }

class Solution {

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        ListNode cur  = new ListNode(2);
        ListNode cur1 = new ListNode(3);
        head.next = cur;
        cur.next  = cur1;
        new Solution().reversePrint(head);
    }


    // 从尾部到头部打印链表, 从尾部 拿出来放到数组中
    // 难点在数组的大小没办法确定,所以我打算用 List 来
    public int[] reversePrint(ListNode head) {
        List<Integer> list = new ArrayList<Integer>();
        helper(head,list);
        return null;
    }

    private void helper(ListNode head,List<Integer> list){
        if(head == null){
            return ;
        }
        helper(head.next,list);
        list.add(head.val);
    }
}