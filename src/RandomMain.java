
import org.junit.jupiter.api.Test;

public class RandomMain {


    // shift + F6 选中相同名称变量同时修改其名称.

    /**
     * 得出结论 : 当自己对自己取模的时候,用 n&(base - 1) 就不正确了
     *          但是 当 n&(n-1) 的到的结果就是消除其最右边的二进制为 1 的位置 
     */
    @Test
    public void test0(){
        System.out.println(50&(50-1)); // - 2
        System.out.println(48&(48-1)); // -16
        System.out.println(32&(32-1));

        int change = 50;
        change = 51;
        change = 45;
        // 自己对自己取模
        System.out.println(50%50); // 不是地
    }
}
