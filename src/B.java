
public class B{
    void meth(){
        System.out.println("我是弗雷 ");
        say();
    }
    void say(){
        System.out.println("我是弗雷的say");
    }

    public static void main(String[] args) {
        B b = new D();
        b.meth();
    }
}
class D extends B{
    @Override
    void say() {
        System.out.println("我是你爹");
    }
}