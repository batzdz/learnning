package com.zdz;

import java.util.Random;

/**
 * 线程的状态
 *
 * NEW    新生
 * RUNNABLE 整在运行
 * BLOCKED 阻塞中
 * WAITING 等待中
 * TIMED_WAITING 时间片用完
 * TERMINATED 中止
 */
// 多线程
public class FellowDemo1 {
    static class MyThread implements Runnable{
        public volatile boolean find_error = true;
        @Override
        public void run() {
            Random random = new Random();
            while(true){// 打印线程名称
                System.out.println(Thread.currentThread().getName());
                try {// 随机停止运行0-9 秒
                    Thread.sleep(random.nextInt(10));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new MyThread());
        Thread t2 = new Thread(new MyThread());
        Thread t3 = new Thread(new MyThread());

        t1.start();
        t2.start();
        t3.start();

        Random random = new Random();
        while(true){
            System.out.println(Thread.currentThread().getName());
            Thread.sleep(random.nextInt(10));
        }
    }
}
