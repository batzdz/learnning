package com.zdz;

public class FellowDemo2 {
    private static final long count = 10_0000_0000;
    static class MyThread extends Thread{
        @Override
        public void run() {
            super.run();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        concurrner();
        serial();
    }

    private static void serial() {
        long begin = System.currentTimeMillis();
        int a = 0;
        for(long i = 0; i < count; i++){
            a++;
        }
        int b = 0;
        for(long i = 0; i < count; i++){
            b--;
        }
        long end = System.currentTimeMillis();

        double ms = (end - begin) *1.0/1000/1000;
        System.out.printf("串行： %f 毫毛\n",ms);
    }

    private static void concurrner() throws InterruptedException {
        long begin = System.currentTimeMillis();
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                int a = 0;
                for(long i = 0; i < count; i++){
                    a++;
                }
            }
        });
        thread.start();
        int b = 0;
        for(long i = 0; i < count; i++){
            b--;
        }
        // 等待thread 运行结束
        thread.join();
        long end = System.currentTimeMillis();

        double ms = (end - begin) *1.0/1000/1000;
        System.out.printf("并发： %f 毫毛\n",ms);
    }
}
