package com.zdz;

// 线程中断的方式1
public class ThreadDemo1 {
    static class MyRunnable implements Runnable{
        public volatile boolean isQuit = false;
        @Override
        public void run() {
            while(!isQuit){
                System.out.println("别烦我我转账呢 ");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(Thread.currentThread().getName()+"啊险些误了大事!");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        MyRunnable target = new MyRunnable();
        Thread thread = new Thread(target,"李四");
        System.out.println("李四开始转账 " + Thread.currentThread().getName());
        thread.start();
        Thread.sleep(5*1000);
        System.out.println(Thread.currentThread().getName() + "老板来电话了对方是骗子!");
        target.isQuit = true;
    }
}
