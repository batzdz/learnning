package com.zdz;

public class FellowDemo3 {
    public static void main(String[] args) {
        Thread thread = new Thread();
        Thread thread1 = new Thread("这是线程的名字为了方便调试");
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {

            }
        },"线程名字");
        System.out.println(thread1.getId());// 获取线程 id
        System.out.println(thread1.getName());// 获取线程名称
        System.out.println(thread2.getState()); // 获取当前状态
        System.out.println(thread2.getPriority());// 获取优先级
        System.out.println(thread2.isDaemon()); // 是不是后台线程
        System.out.println(thread1.isAlive()); // 是否存活
        System.out.println(thread1.isInterrupted());// 是不是被中断了
    }
}
