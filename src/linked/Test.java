package linked;

public class Test {
    private static void assertTrue(boolean condition) {
        if (!condition) {
            throw new RuntimeException();
        }
    }
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.check();

//        list.add(90,100);
//        list.check();
//        assertTrue(list.size() == 1);

        assertTrue(list.size() == 0);
        list.add(100);
        list.check();
        assertTrue(list.size() == 1);
        list.add(200);
        list.check();
        assertTrue(list.size() == 2);
        list.add(1,250);
        list.check();
        assertTrue(list.size() == 3);

        assertTrue( list.remove(0) == 100);
        assertTrue(list.size() == 2);
        list.add(300);
        list.check();
        assertTrue(list.size() == 3);
        list.add(400);
        list.check();
        assertTrue(list.size() == 4);


        assertTrue(list.remove(3)== 400);

    }
}
