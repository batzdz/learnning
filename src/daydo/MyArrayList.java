package daydo;


import java.util.Arrays;

public class MyArrayList {
    private long[] array;
    private int size;

    public MyArrayList() {
        array = new long[2];
        size = 0;
        Arrays.fill(array, Long.MIN_VALUE);
    }

    public void add(long e) {
        ensureCapacity();
        array[size] = e;
        size++;
    }

    public int size() {
        return size;
    }

    public long get(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException(index + ": " + size);
        }

        return array[index];
    }

    public void check() {
        if (array == null) {
            throw new RuntimeException();
        }
        if (array.length == 0) {
            throw new RuntimeException();
        }
        if (size < 0) {
            throw new RuntimeException();
        }
        if (size > array.length) {
            throw new RuntimeException();
        }
        for (int i = 0; i < size; i++) {
            if (array[i] == Long.MIN_VALUE) {
                throw new RuntimeException();
            }
        }
        // 检查 [size, array.length) 存入的全部是无效值
        for (int i = size; i < array.length; i++) {
            if (array[i] != Long.MIN_VALUE) {
                throw new RuntimeException();
            }
        }
    }

    public void add(int index, long e) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException(index + ": " + size);
        }

        ensureCapacity();

        for (int i = size - 1; i >= index; i--) {
            int j = i + 1;
            array[j] = array[i];
        }

        array[index] = e;
        size++;
    }

    private void ensureCapacity() {
        if (size < array.length) {
            return;
        }

        array = Arrays.copyOf(array, array.length * 2);
        Arrays.fill(array, size, array.length, Long.MIN_VALUE);
    }

    // O(n)
    public void delete(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException(index + ": " + size);
        }

        for (int f = index + 1; f < size; f++) {
            int t = f - 1;
            array[t] = array[f];
        }

        size--;
        array[size] = Long.MIN_VALUE;
    }

    // O(n)
    public void removeFromHead(long e) {
        int index = 0;
        while (index < size) {
            if (array[index] == e) {
                break;
            }
            index++;
        }

        // 怎么区分是否找到
        if (index == size) {
            // 没有找到
            return;
        }

        for (int f = index + 1; f < size; f++) { // 将元素向上放
            int t = f - 1;
            array[t] = array[f];
        }

        size--;
        array[size] = Long.MIN_VALUE;
    }

    // O(n)
    public void removeFromLast(long e) {
        int index = size - 1;
        while (index >= 0) {
            if (array[index] == e) {
                break;
            }

            index--;
        }

        if (index == -1) {
            return;
        }


        for (int f = index + 1; f < size; f++) {
            int t = f - 1;
            array[t] = array[f];
        }

        size--;
        array[size] = Long.MIN_VALUE;
    }


    // 这个写到博客中
    public void removeAll(long e){ // 删除所有元素值 == e 元素
        int to = 0;
        int f = 0;
        for(; f < size; f++){
            if (array[f] != e){
                array[to] = array[f];
                to++; // == e 的时候 to 就留在了 e 出现的位置
            }
        }
            Arrays.fill(array,to,f,Long.MIN_VALUE);
            size = to;
    }

}
