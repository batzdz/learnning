package daydo;

public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int val) {
        this.val = val;
    }

    public static void main(String[] args) {
        ListNode node0 = new ListNode(4);
        ListNode node1 = new ListNode(3);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(1);
        node0.next = node1;
        node1.next = node2;
        node2.next = node3;
//        print(node0);
//        print_n(2,node0);
//        print_end(node0);
        print(deleteHead(node0));
    }
    // 从后向前遍历每一个节点
    static void print(ListNode head){
        if(head == null){
            return;
        }
        print(head.next);
        System.out.println(head.val);
    }

    static void print_n(int n,ListNode head){
        for(;n > 0; n--){
            head = head.next;
        }
        while(head != null){
            System.out.println(head.val);
            head = head.next;
        }
    }

    static void print_end(ListNode head){
        if(head == null || head.next == null) return;
        while(head.next != null){
            System.out.println(head.val);
            head = head.next;
        }
    }
    static ListNode deleteHead(ListNode head){
        if(head == null) return null;
        ListNode ans = head.next;
        head.next = null;
        return ans;
    }
}