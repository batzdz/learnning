package daydo;


public class UseArrayList {
    // assert ：断言
    private static void assertTrue(boolean condition) {
        if (!condition) {
            throw new RuntimeException();
        }
    }

    public static void main(String[] args) {
        MyArrayList arrayList = new MyArrayList();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(5);
        arrayList.add(4);
        arrayList.add(3);
        arrayList.add(2);
        arrayList.add(1);
        arrayList.check();
        assertTrue(arrayList.size() == 10);

        // 0 1 2 3 4 5 6 7
        // 1 3 4 5 5 4 3 1
        arrayList.removeAll(2);
        arrayList.check();
        assertTrue(arrayList.size() == 8);
        assertTrue(arrayList.get(0) == 1);
        assertTrue(arrayList.get(1) == 3);
        assertTrue(arrayList.get(2) == 4);
        assertTrue(arrayList.get(6) == 3);
        assertTrue(arrayList.get(7) == 1);
    }

    public static void main3(String[] args) {
        MyArrayList arrayList = new MyArrayList();
        for (int i = 0; i < 10; i++) {
            arrayList.add(i + 1);
        }
        arrayList.check();
        assertTrue(arrayList.size() == 10);

        arrayList.delete(-1);

        // [9]   [0]   [4]   [-1]   [19]
        arrayList.delete(9);
        arrayList.check();
        assertTrue(arrayList.size() == 9);
        assertTrue(arrayList.get(0) == 1);
        assertTrue(arrayList.get(8) == 9);

        arrayList.delete(0);
        arrayList.check();
        assertTrue(arrayList.size() == 8);
        assertTrue(arrayList.get(0) == 2);
        assertTrue(arrayList.get(1) == 3);
        assertTrue(arrayList.get(2) == 4);
        assertTrue(arrayList.get(3) == 5);
        assertTrue(arrayList.get(4) == 6);
        assertTrue(arrayList.get(5) == 7);
        assertTrue(arrayList.get(6) == 8);
        assertTrue(arrayList.get(7) == 9);

        arrayList.delete(4);
        arrayList.check();
        assertTrue(arrayList.size() == 7);
        assertTrue(arrayList.get(0) == 2);
        assertTrue(arrayList.get(1) == 3);
        assertTrue(arrayList.get(2) == 4);
        assertTrue(arrayList.get(3) == 5);
        assertTrue(arrayList.get(4) == 7);
        assertTrue(arrayList.get(5) == 8);
        assertTrue(arrayList.get(6) == 9);
    }

    public static void main2(String[] args) {
        MyArrayList arrayList = new MyArrayList();
        arrayList.check();
        assertTrue(arrayList.size() == 0);

        arrayList.add(0, 100);
        arrayList.check();
        assertTrue(arrayList.size() == 1);
        assertTrue(arrayList.get(0) == 100);

        arrayList.add(0, 200);
        arrayList.check();
        assertTrue(arrayList.size() == 2);
        assertTrue(arrayList.get(0) == 200);
        assertTrue(arrayList.get(1) == 100);

        arrayList.add(1, 300);
        arrayList.check();
        assertTrue(arrayList.size() == 3);
        assertTrue(arrayList.get(0) == 200);
        assertTrue(arrayList.get(1) == 300);
        assertTrue(arrayList.get(2) == 100);

        arrayList.add(3, 400);
        arrayList.check();
        assertTrue(arrayList.size() == 4);
        assertTrue(arrayList.get(0) == 200);
        assertTrue(arrayList.get(1) == 300);
        assertTrue(arrayList.get(2) == 100);
        assertTrue(arrayList.get(3) == 400);
    }

    public static void main1(String[] args) {
        MyArrayList arrayList = new MyArrayList();  // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 0);


        arrayList.add(1);                           // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 1);
        assertTrue(arrayList.get(0) == 1);


        arrayList.add(2);                           // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 2);
        assertTrue(arrayList.get(0) == 1);
        assertTrue(arrayList.get(1) == 2);


        arrayList.add(3);                           // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 3);
        assertTrue(arrayList.get(0) == 1);
        assertTrue(arrayList.get(1) == 2);
        assertTrue(arrayList.get(2) == 3);


        arrayList.add(4);                           // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 4);
        assertTrue(arrayList.get(0) == 1);
        assertTrue(arrayList.get(1) == 2);
        assertTrue(arrayList.get(2) == 3);
        assertTrue(arrayList.get(3) == 4);


        arrayList.add(5);                           // 满足一致性
        arrayList.check();
        assertTrue(arrayList.size() == 5);
        assertTrue(arrayList.get(0) == 1);
        assertTrue(arrayList.get(1) == 2);
        assertTrue(arrayList.get(2) == 3);
        assertTrue(arrayList.get(3) == 4);
        assertTrue(arrayList.get(4) == 5);
    }
}
