package http.dtail;

import http.ach.HttpServerV2;

import javax.print.DocFlavor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class HttpRequest {
    private String method;
    private String url;
    private String version;

    //请求头 (包含一些属性的信息 cookie content-type ...)信息
    private Map<String,String> headers = new HashMap<>();
    // url 中的参数信息,和 body 中的参数都放到这个 parameters HashMap中
    private Map<String,String> parameters = new HashMap<>();
    // 本来 cookie信息 是放在请求头中 但是内容过多 所以就单独 拿出来保存；
    private Map<String, String> cookies = new HashMap<>();
    private String body;

    public static HttpRequest build(InputStream inputStream) throws IOException {
        HttpRequest  request = new HttpRequest();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        //1. 处理首行
        String firstLine = bufferedReader.readLine();
        String[] firstLineTokens = firstLine.split(" ");
        request.method = firstLineTokens[0];
        request.url = firstLineTokens[1];
        request.version = firstLineTokens[2];

        // 2.解析 url
        int pos = request.url.indexOf("?");
        if(pos != -1){
            String queryString = request.url.substring(pos+1);
            parseKV(queryString,request.parameters);
        }

        // 3.循环处理header 部分
        String line = "";
        while((line = bufferedReader.readLine()) != null && line.length() != 0 ){
            String[] headersTokens = line.split(": ");
            request.headers.put(headersTokens[0],headersTokens[1]);
        }
        // 4. 解析 cookie
        String cookie = request.headers.get("Cookie");
        if(cookie != null){
            // 把 cookie 进行解析
            parseCookie(cookie,request.cookies);
        }

        // 5. 解析 body
        if("Post".equalsIgnoreCase(request.method) || "PUT".equalsIgnoreCase(request.method)){
            // 需要处理 body
            int contentLength = Integer.parseInt(request.headers.get("Content-Length"));
            // 如果 contentLength = 100 字节 ；那么 缓冲区域的长度就是 100 字符 ，相当于 200 字节
            char[] buffer = new char[contentLength];
            int len = bufferedReader.read(buffer);
            // body 中的 形式 如 : username=张三&password=123;
            request.body = new String(buffer,0,len);
            parseKV(request.body,request.parameters);
        }
        return request;
    }

    private static void parseCookie(String cookie, Map<String, String> cookies) {
        // 1. 按照 ;空格 区分 多个键值对
        String[] kvTokens = cookie.split("; ");
        // 2. 按照 = 拆分成 每个键和值
        for(String kv :  kvTokens){
            String[] result = kv.split("=");
            cookies.put(result[0],result[1]);
        }
    }

    private static void parseKV(String queryString, Map<String, String> parameters) {
        // 1. 按照 & 拆分成多个键值对
        String[] kvTokens = queryString.split("&");
        // 2. 按照 = 拆分成 每个 键 和 值
        for(String kv : kvTokens){
            String[] result = kv.split("=");
            parameters.put(result[0],result[1]);
        }
    }

    public String getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public String getVersion() {
        return version;
    }

    public String getHeaders(String key) {
        return headers.get(key);
    }

    public String getParameters(String key) {
        return parameters.get(key);
    }

    public String getCookies(String key) {
        return cookies.get(key);
    }

    public String getBody() {
        return body;
    }
}
