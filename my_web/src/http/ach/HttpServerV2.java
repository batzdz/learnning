package http.ach;

// 主要是为了Cookie 的实现
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HttpServerV2 {
    private ServerSocket serverSocket = null;

    public HttpServerV2(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void start() throws IOException {
        System.out.println("服务器启动");
        ExecutorService executorService = Executors.newCachedThreadPool();
        while(true){
            Socket clientSocket = serverSocket.accept();
            executorService.execute(new Runnable(){
                @Override
                public void run() {
                    process(clientSocket);
                }
            });
        }
    }


    // 业务处理在这里实现
    private void process(Socket clientSocket){
        try {
            HttpRequest request = HttpRequest.build(clientSocket.getInputStream());
            HttpResponse response = HttpResponse.build(clientSocket.getOutputStream());
            if("/hello".equals(request.getUrl())){
                response.setMessage("OK");
                response.setStatus(200);
                response.setBody("<h1>你好</h1>");
            }else if("/redirect".equals(request.getUrl())){
                response.setStatus(307);
                response.setMessage("Temporary Redirect");
                response.setHeaders("Location","https://www.baidu.com");
            }else if(request.getUrl().startsWith("/notfound")) {
                response.setMessage("Not Found");
                response.setStatus(404);
                response.setBody("<h1>啥也没有</h1>");
            }else if(request.getUrl().startsWith("/UserCookies")){
                response.setMessage("OK");
                response.setStatus(200);
                response.setHeaders("Set-Cookie","user=zdz;");
                response.setBody("<h1>设置用户cookies</h1>");
            }else if(request.getUrl().startsWith("/TimeCookies")){
                response.setMessage("OK");
                response.setStatus(200);
                response.setHeaders("Set-Cookie","time="+(System.currentTimeMillis()/1000));
                response.setBody("<h1>设置时间cookies</h1>");
            }else if(request.getUrl().startsWith("/calc")){
                int pos = request.getUrl().indexOf("?");
                String variable = request.getUrl().substring(pos+1);
                String[] varTokens = variable.split("&");
                int ans = 0;
                for(String token : varTokens){
                    ans += Integer.parseInt(token.split("=")[1]);
                }
                response.setMessage("OK");
                response.setStatus(200);
                response.setBody("<h1>sum is " + ans +"</h1>");
            } else {
                response.setMessage("Not Found");
                response.setStatus(404);
                response.setBody("<h1>啥也没有</h1>");
            }

            response.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        HttpServerV2 httpServerV2 = new HttpServerV2(9090);
        httpServerV2.start();
    }
}
