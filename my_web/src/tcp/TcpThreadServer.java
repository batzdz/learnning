package tcp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpThreadServer {
    private ServerSocket serverSocket = null;

    public TcpThreadServer(int port) throws IOException {
        this.serverSocket = new ServerSocket(port);
    }

    private void processConnection(Socket clientSocket){
        System.out.printf("[%s:%d] 连接服务器\n",clientSocket.getInetAddress().toString(),clientSocket.getPort());
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()))){
            while (true) {
                String request = bufferedReader.readLine();
                String response = process(request);
                bufferedWriter.write(response+"\n");
                bufferedWriter.flush();
                System.out.printf("[%s:%d] req: %s; resp: %s;\n",clientSocket.getInetAddress().toString(),clientSocket.getPort(),request,response);
            }
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.printf("[%s:%d] 下线 !\n",clientSocket.getInetAddress().toString(),clientSocket.getPort());
        }
    }

    private String process(String request) {
        return request;
    }

    public void start() throws IOException {
        System.out.println("服务器启动");
        while(true){
            Socket clientSocket = serverSocket.accept();
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    processConnection(clientSocket);
                }
            });
            thread.start();
        }
    }

    public static void main(String[] args) throws IOException {
        TcpThreadServer tcpThreadServer = new TcpThreadServer(9191);
        tcpThreadServer.start();
    }
}
