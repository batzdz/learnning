package udp;

import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

public class UdpDictServer extends UdpEchoServer{
    private Map<String,String> map = new HashMap<>();
    public UdpDictServer(int port) throws SocketException {
        super(port);
        map.put("cat","猫");
        map.put("dog","狗");
        map.put("fuck","干你");
        map.put("kick","打你");
    }

    @Override
    protected String process(String request) {
        return map.getOrDefault(request,"你个伞兵 别问老子不会的");
    }

    public static void main(String[] args) throws IOException {
        UdpDictServer udpDictServer = new UdpDictServer(9090);
        udpDictServer.start();
    }
}
